+++
title = "About Me"
description = "About DJ Yuhn"
date = 2021-10-20
noLicense = true
showWordCounter = false
showReadTime = false
+++

# Hey there, I'm DJ
I am a software developer, and a former medical technologist, based in Kansas City. I have worked with a variety of technologies including: Azure, Azure DevOps, .NET, Angular, and Typescript. I have been working as a software developer since April 2019, and I completed my master's degree in Computer Science in December 2019.

During the day I work on Windows but at home I use a Linux distro called [Solus](https://getsol.us/home/).

***

# Skills

## Languages:
* C#
* Kotlin
* Typescript
* Java
* Python

## Database:
* MySQL
* MongoDB
* PostgreSQL

## Cloud:
* Azure

## Tools
* Visio
* Git
* Postman
* Linqpad
* Jetbrains Suite
* Visual Studio

## Frameworks
* .NET
* Standford CoreNLP
* Apache Spark
* Angular

## Virtualization
* Docker
* Oracle VirtualBox

***

# Currently Using

**Computer:** Lenovo X1 Carbon 6th Gen

**Hosting:** [Netlify](https://www.netlify.com/)

**OS**: [Solus](https://getsol.us/home/)
