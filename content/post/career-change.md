+++
title = "Career Change to Software Developer"
date = 2020-02-11
description = "I'm DJ Yuhn, and I made a career change to become a software developer. I was working as a medical technologist before then and decided to return to school for a master's degree in computer science."
tags = ["life", "career"]
+++

# Start

I was born in 1991 and grew up in a small town in Kansas. I spent most of my childhood playing video games on the SNES, N64, Xbox, and PC. My first exposure to the internet was playing Starcraft on my Opa's computer in 1998. I remember being able to log on and play against others in 1-vs-1 matches and in teams for 4-vs-4 matches. I then discovered custom map types, where I remember playing the ['Cat N Mouse'](https://www.youtube.com/watch?v=AfuX5dXSUE8) games over and over, all night, every night. Little me was blown away that you could make a game within a game and I wanted to learn to make my own custom games. I booted up StarEdit and started doing random things. I had absolutely no idea how to make triggers work or how to edit the units' stats, but I was hooked. I enjoyed just sitting there and playing Starcraft and making maps by tweaking the custom games like 'Cat N Mouse', or 'Diplomacy' to put my own spin on it and to share it with these random strangers online. This is what got me interested in tech.

<!--more-->

I continued humming along like this, just playing games through grade school, middle school, and up until about senior year of high school. In that year I became interested in graphic design and I thought it would be the career for me. When I graduated high school I enrolled at the University of Kansas (KU) seeking a graphic design degree and I didn't even make it a full semester until I realized I do not like making these books by hand, learning about these gestalt principles, and being graded subjectively. I then decided I wanted to do microbiology and went down that path.

# Wrong career

After struggling paying for school at KU, I worked as a garbage man for awhile to save up money and then transferred over to Kansas State University (KSU) as they offered up some scholarships. At KSU I continued working towards my microbiology degree and my clinical laboratory science degree. I moved to Kansas City for that final year to do a year long program, through KSU, at Saint Luke's Hospital. This program was mandatory to earn a degree in clinical laboratory science at KSU and to be eligible to sit for certification with the American Society of Clinical Pathologists (ASCP). I completed the program, earned my microbiology and clinical laboratory science degrees and became a certified medical technologist in 2015.

Once I began working as a medical technologist I found myself wanting something more challenging and less monotonous. At some point, I reflected back at those times playing Starcraft and making those maps. Programming the units to go this way and that. I eventually decided I wanted to look into programming and I took up learning Python.

# Right career

While working night shifts in the laboratory I saved up my money to return to school and kept fiddling with Python. I eventually decided to dive in and enrolled at the University of Missouri - Kansas City (UMKC) to take deficiency courses to meet the requirements for their master's program for computer science. I quit my night shift job and started focusing on school. I was enjoying some aspects of the class work but my mind was fixated on thinking I was making the wrong decision. That I should've been content with being a medical technologist and that I would ultimately have to go back to the lab. I kept through it with the help of my girlfriend and after a couple of these deficiency courses I reached the point where I could start enrolling in the master's program.

Once I started taking the graduate level courses I really began looking for internships and jobs related to computer science. I also was fortunate enough to find a position in a laboratory that wasn't a night shift position and that didn't interfere with my class schedule. I tried going to the school career center, the career fairs, reaching out on Reddit for resume critiques, and asking students and professors about opportunities as a software developer. Fortunately, a recruiter from a career fair remembered me and reached out to ask if I would be interested in a machine learning internship at IPFS in Kansas City. A quick interview later and I became an intern!

At this point I was working at the lab, doing the internship, and attending my graduate classes. I felt completely overwhelmed but I also felt that I was finally going somewhere. The internship lasted through winter break and continued on partially through that spring semester of 2019. It taught me a bit on how the industry approaches software development, specifying requirements, and communicating with other professionals in the industry. In March of 2019 I was contacted by a recruiter at Artisan Technology group for a Software Engineer position. I was in disbelief and ecstatic about being contacted directly for a real full-time position here in Kansas City. I did the coding test, a remote interview, and an onsite interview. A day or two later after the onsite and I received the job offer! I put in my two weeks notice at the internship and at the laboratory and started my first job with the title of Software Engineer in April 2019. 

Since taking up the job I've been involved with full-stack development work in various domains and tackling interesting problems. Every day is a challenge but it's a welcomed challenge. I am incredibly fortunate to have been provided the opportunity to experience the journey and to find myself on a path that reflects what I would like in a career.
