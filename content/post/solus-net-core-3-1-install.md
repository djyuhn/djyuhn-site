+++
title = "Install .NET Core 3.1 on Solus using Snap"
date = 2020-02-29
description = "Installing .NET Core 3.1 on Solus with the use of Snap!"
tags = ["solus", ".net"]
+++

# Overview

This post is to provide instructions on how .NET Core 3.1 was installed on my machine running Solus 4.1. With the use of Snap these instructions may be followed on other Linux distributions provided that [Snap is installed](https://snapcraft.io/docs/installing-snapd). The steps will involve obtaining the .NET Core 3.1 Snap using the terminal and then creating an alias.

<!--more-->

# Instructions

In a terminal invoke `sudo snap install dotnet-sdk --channel=3.1/stable --classic`. This will download and install the snap. Once the dotnet-sdk snap is installed use `dotnet-sdk.dotnet` to verify installation. Now an alias will be created to run `dotnet-sdk.dotnet` using `dotnet`. This will be done by performing the following: `sudo snap alias dotnet-sdk.dotnet dotnet`. Once complete then .NET Core 3.1 will be installed.

1. Install -> `sudo snap install dotnet-sdk --channel=3.1/stable --classic`
2. Verify -> `dotnet-sdk.dotnet`
3. Create Alias -> `sudo snap alias dotnet-sdk.dotnet dotnet`

<video controls autoplay style="width: 100%">
  <source src="/media/install_dotnet.mp4" type="video/mp4" />
</video>

# Conclusion

This method should allow for the use of .NET Core 3.1 on the system. Once this was installed Jetbrains Rider recognized the SDK and provided the ability to create new solutions and open existing projects without errors.

![Jetbrains Rider](/media/dotnet_core.png)
